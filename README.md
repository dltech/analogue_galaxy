# analogue galaxy - a galaxy of analog devices. A collection of retro stylish gadgets.

# truenixie
UFD clock w display from soviet calculator\
![eee](truenixie/pcb/sch_small.png)
![ee](truenixie/pcb/pcb3d.png)
![e](truenixie/pcb/pcb3db.png)

# pseudonixie
An imitation of UFD lamp with oled display.\
![eee](pseudonixie/pcb/sch_small.png)
![ee](pseudonixie/pcb/pcb3d.png)
![e](pseudonixie/pcb/pcb3db.png)

# aystm32
An old ZXspectrum sound chip sound card.\
![eee](aystm32/pcb/pcb_small.png)
![ee](aystm32/pcb/aydongle3d.png)
![e](aystm32/pcb/aydongle3db.png)

# aystm32
LED clock, ufd imitation.\
![eee](electronika_clock/mini/sch.png)
![eee](electronika_clock/mini/pcb3d.png)



